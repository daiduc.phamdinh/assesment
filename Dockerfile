FROM python:3.6

COPY src/ /usr/src/app/
WORKDIR /usr/src/app/

RUN ls && pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD python app.py
